/* sim-safe.c - sample functional simulator implementation */

/* SimpleScalar(TM) Tool Suite
* Copyright (C) 1994-2003 by Todd M. Austin, Ph.D. and SimpleScalar, LLC.
* All Rights Reserved. 
* 
* THIS IS A LEGAL DOCUMENT, BY USING SIMPLESCALAR,
* YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.
* 
* No portion of this work may be used by any commercial entity, or for any
* commercial purpose, without the prior, written permission of SimpleScalar,
* LLC (info@simplescalar.com). Nonprofit and noncommercial use is permitted
* as described below.
* 
* 1. SimpleScalar is provided AS IS, with no warranty of any kind, express
* or implied. The user of the program accepts full responsibility for the
* application of the program and the use of any results.
* 
* 2. Nonprofit and noncommercial use is encouraged. SimpleScalar may be
* downloaded, compiled, executed, copied, and modified solely for nonprofit,
* educational, noncommercial research, and noncommercial scholarship
* purposes provided that this notice in its entirety accompanies all copies.
* Copies of the modified software can be delivered to persons who use it
* solely for nonprofit, educational, noncommercial research, and
* noncommercial scholarship purposes provided that this notice in its
* entirety accompanies all copies.
* 
* 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
* PROHIBITED WITHOUT A LICENSE FROM SIMPLESCALAR, LLC (info@simplescalar.com).
* 
* 4. No nonprofit user may place any restrictions on the use of this software,
* including as modified by the user, by any other authorized user.
* 
* 5. Noncommercial and nonprofit users may distribute copies of SimpleScalar
* in compiled or executable form as set forth in Section 2, provided that
* either: (A) it is accompanied by the corresponding machine-readable source
* code, or (B) it is accompanied by a written offer, with no time limit, to
* give anyone a machine-readable copy of the corresponding source code in
* return for reimbursement of the cost of distribution. This written offer
* must permit verbatim duplication by anyone, or (C) it is distributed by
* someone who received only the executable form, and is accompanied by a
* copy of the written offer of source code.
* 
* 6. SimpleScalar was developed by Todd M. Austin, Ph.D. The tool suite is
* currently maintained by SimpleScalar LLC (info@simplescalar.com). US Mail:
* 2395 Timbercrest Court, Ann Arbor, MI 48105.
* 
* Copyright (C) 1994-2003 by Todd M. Austin, Ph.D. and SimpleScalar, LLC.
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#include "host.h"
#include "misc.h"
#include "machine.h"
#include "regs.h"
#include "memory.h"
#include "loader.h"
#include "syscall.h"
#include "options.h"
#include "stats.h"
#include "sim.h"


#define OneBit_predictor_flag FALSE
#define TOTAL_SIZE  262144//32768 // 262,144 bits of storage
#define LOG_ONE_BIT_TAG_BITS  18//15 // log base 2 of 262144 = 18

#define TwoBit_sc_predictor_flag  FALSE
#define TWOBIT_SC_TAG_BITS 17 //log base 2 of 262144/2 = 17

#define OneBit_c_predictor_flag FALSE
#define ONEBIT_C_TAG_BITS 17 // log base 2 of (262144/2) = 17


#define TwoBit_FourBitC_predictor_flag FALSE
#define TWOBIT_4BIT_SC_TAG_BITS 13 // log base 2 of 262144/2/16 = 13

#define gshare_predictor_flag TRUE
#define GSHARE_TAG_BITS 17 // log base 2 of 262144/2/16 = 13


// Do not set the below to TRUE, I was trying to make the all prediction work at the same time. 
#define test_all FALSE


/*
* This file implements a functional simulator.  This functional simulator is
* the simplest, most user-friendly simulator in the simplescalar tool set.
* Unlike sim-fast, this functional simulator checks for all instruction
* errors, and the implementation is crafted for clarity rather than speed.
*/

/* simulated registers */
static struct regs_t regs;

/* simulated memory */
static struct mem_t *mem = NULL;

/* track number of refs */
static counter_t sim_num_refs = 0;

/* maximum number of inst's to execute */
static unsigned int max_insts;

static float accuracy[5]={0.0,0.0,0.0,0.0,0.0};
static counter_t branches[5]={0,0,0,0,0};
static counter_t misbranches[5]={0,0,0,0,0};


  counter_t g_total_cond_branches = 0;
counter_t g_total_mispredictions = 0;

/* register simulator-specific options */
void
sim_reg_options(struct opt_odb_t *odb)
{
	opt_reg_header(odb, 
	"sim-safe: This simulator implements a functional simulator.  This\n"
	"functional simulator is the simplest, most user-friendly simulator in the\n"
	"simplescalar tool set.  Unlike sim-fast, this functional simulator checks\n"
	"for all instruction errors, and the implementation is crafted for clarity\n"
	"rather than speed.\n"
	);

	/* instruction limit */
	opt_reg_uint(odb, "-max:inst", "maximum number of inst's to execute",
	&max_insts, /* default */0,
	/* print */TRUE, /* format */NULL);

}


/* check simulator-specific option values */
void
sim_check_options(struct opt_odb_t *odb, int argc, char **argv)
{
	/* nada */
}

/* register simulator-specific statistics */
void
sim_reg_stats(struct stat_sdb_t *sdb)
{
	stat_reg_counter(sdb, "sim_num_insn",
	"total number of instructions executed",
	&sim_num_insn, sim_num_insn, NULL);
	stat_reg_counter(sdb, "sim_num_refs",
	"total number of loads and stores executed",
	&sim_num_refs, 0, NULL);
	stat_reg_int(sdb, "sim_elapsed_time",
	"total simulation time in seconds",
	&sim_elapsed_time, 0, NULL);
	stat_reg_formula(sdb, "sim_inst_rate",
	"simulation speed (in insts/sec)",
	"sim_num_insn / sim_elapsed_time", NULL);


#if test_all==TRUE
	int count = 0 ;
	for (count = 0 ; count<5 ; count++){
		stat_reg_float(sdb, "stat.a_float", "Prediction Accuracy.",
		 &accuracy[count],accuracy[count], NULL);
	}
	for (count = 0 ; count<5 ; count++){
		stat_reg_counter(sdb, "sim_num_cond_branches",
		"total number of conditional branches executed",
		&branches[count],	branches[count], NULL);
		stat_reg_counter(sdb, "sim_num_cond_branches",
		"total number of conditional branches misdirected",
		&	misbranches[count],	misbranches[count], NULL);
	}
#else 
	stat_reg_counter(sdb, "sim_num_cond_branches",
	"total number of conditional branches executed",
	&g_total_cond_branches,
	g_total_cond_branches, NULL);
	stat_reg_counter(sdb, "sim_num_mispredict",
	"total number of conditional branches misdirected",
	&g_total_mispredictions,
	g_total_mispredictions, NULL);
	stat_reg_formula(sdb, "sim_pred_accuracy",
	"branch prediction accuracy",
	"1 - sim_num_mispredict / sim_num_cond_branches", NULL);
#endif	
	ld_reg_stats(sdb);
	mem_reg_stats(mem, sdb);
}

/* initialize the simulator */
void
sim_init(void)
{
	sim_num_refs = 0;

	/* allocate and initialize register file */
	regs_init(&regs);

	/* allocate and initialize memory space */
	mem = mem_create("mem");
	mem_init(mem);
}

/* load program into simulated state */
void
sim_load_prog(char *fname,		/* program to load */
int argc, char **argv,	/* program arguments */
char **envp)		/* program environment */
{
	/* load program text and data, set up environment, memory, and regs */
	ld_load_prog(fname, argc, argv, envp, &regs, mem, TRUE);
}

/* print simulator-specific configuration information */
void
sim_aux_config(FILE *stream)		/* output stream */
{
	/* nothing currently */
}

/* dump simulator-specific auxiliary simulator statistics */
void
sim_aux_stats(FILE *stream)		/* output stream */
{
	/* nada */
}

/* un-initialize simulator-specific state */
void
sim_uninit(void)
{
	/* nada */
}


/*
* configure the execution engine
*/

/*
* precise architected register accessors
*/

/* next program counter */
#define SET_NPC(EXPR)		(regs.regs_NPC = (EXPR))

/* current program counter */
#define CPC			(regs.regs_PC)

/* general purpose registers */
#define GPR(N)			(regs.regs_R[N])
#define SET_GPR(N,EXPR)		(regs.regs_R[N] = (EXPR))

#if defined(TARGET_PISA)

/* floating point registers, L->word, F->single-prec, D->double-prec */
#define FPR_L(N)		(regs.regs_F.l[(N)])
#define SET_FPR_L(N,EXPR)	(regs.regs_F.l[(N)] = (EXPR))
#define FPR_F(N)		(regs.regs_F.f[(N)])
#define SET_FPR_F(N,EXPR)	(regs.regs_F.f[(N)] = (EXPR))
#define FPR_D(N)		(regs.regs_F.d[(N) >> 1])
#define SET_FPR_D(N,EXPR)	(regs.regs_F.d[(N) >> 1] = (EXPR))

/* miscellaneous register accessors */
#define SET_HI(EXPR)		(regs.regs_C.hi = (EXPR))
#define HI			(regs.regs_C.hi)
#define SET_LO(EXPR)		(regs.regs_C.lo = (EXPR))
#define LO			(regs.regs_C.lo)
#define FCC			(regs.regs_C.fcc)
#define SET_FCC(EXPR)		(regs.regs_C.fcc = (EXPR))

#elif defined(TARGET_ALPHA)

/* floating point registers, L->word, F->single-prec, D->double-prec */
#define FPR_Q(N)		(regs.regs_F.q[N])
#define SET_FPR_Q(N,EXPR)	(regs.regs_F.q[N] = (EXPR))
#define FPR(N)			(regs.regs_F.d[(N)])
#define SET_FPR(N,EXPR)		(regs.regs_F.d[(N)] = (EXPR))

/* miscellaneous register accessors */
#define FPCR			(regs.regs_C.fpcr)
#define SET_FPCR(EXPR)		(regs.regs_C.fpcr = (EXPR))
#define UNIQ			(regs.regs_C.uniq)
#define SET_UNIQ(EXPR)		(regs.regs_C.uniq = (EXPR))

#else
#error No ISA target defined...
#endif

/* precise architected memory state accessor macros */
#define READ_BYTE(SRC, FAULT)						\
	((FAULT) = md_fault_none, addr = (SRC), MEM_READ_BYTE(mem, addr))
#define READ_HALF(SRC, FAULT)						\
	((FAULT) = md_fault_none, addr = (SRC), MEM_READ_HALF(mem, addr))
#define READ_WORD(SRC, FAULT)						\
	((FAULT) = md_fault_none, addr = (SRC), MEM_READ_WORD(mem, addr))
#ifdef HOST_HAS_QWORD
#define READ_QWORD(SRC, FAULT)						\
	((FAULT) = md_fault_none, addr = (SRC), MEM_READ_QWORD(mem, addr))
#endif /* HOST_HAS_QWORD */

#define WRITE_BYTE(SRC, DST, FAULT)					\
	((FAULT) = md_fault_none, addr = (DST), MEM_WRITE_BYTE(mem, addr, (SRC)))
#define WRITE_HALF(SRC, DST, FAULT)					\
	((FAULT) = md_fault_none, addr = (DST), MEM_WRITE_HALF(mem, addr, (SRC)))
#define WRITE_WORD(SRC, DST, FAULT)					\
	((FAULT) = md_fault_none, addr = (DST), MEM_WRITE_WORD(mem, addr, (SRC)))
#ifdef HOST_HAS_QWORD
#define WRITE_QWORD(SRC, DST, FAULT)					\
	((FAULT) = md_fault_none, addr = (DST), MEM_WRITE_QWORD(mem, addr, (SRC)))
#endif /* HOST_HAS_QWORD */

/* system call handler macro */
#define SYSCALL(INST)	sys_syscall(&regs, mem_access, mem, INST, TRUE)

#define DNA         (0)

/* general register dependence decoders */
#define DGPR(N)         (N)
#define DGPR_D(N)       ((N) &~1)

/* floating point register dependence decoders */
#define DFPR_L(N)       (((N)+32)&~1)
#define DFPR_F(N)       (((N)+32)&~1)
#define DFPR_D(N)       (((N)+32)&~1)

/* miscellaneous register dependence decoders */
#define DHI         (0+32+32)
#define DLO         (1+32+32)
#define DFCC            (2+32+32)
#define DTMP            (3+32+32)


/*
This function uses one bit branch prediction method 
*/
void OneBit_predictor (int * bpred_pht ,enum md_opcode op)
{
	/*
	We only need to predict the direction (taken, not taken) for conditional branches
	*/
	if( MD_OP_FLAGS(op) & F_COND ) {
		g_total_cond_branches++;
		unsigned index = (regs.regs_PC >> 3) & ((1<<LOG_ONE_BIT_TAG_BITS) -1);
		assert( index < TOTAL_SIZE );
		int prediction = bpred_pht[ index ];
		/* 
		We check what the correct outcome is by comparing the next PC against the sequentially next PC value. 
		*/
		int actual_outcome=(regs.regs_NPC!=(regs.regs_PC+sizeof(md_inst_t)));
		/*
		If the actual branch outcome computed in Step(e) is different from the predicted outcome in Step(d), we mispredicted the branch.
		*/
		if( prediction != actual_outcome ) 
		{g_total_mispredictions++;}
		bpred_pht[ index ] = actual_outcome;
	}
}


/*
This function uses two bit branch prediction method with saturating counter 
*/
void TwoBit_sc_predictor(int * bpred_pht, enum md_opcode op)
{
	/*
	We only need to predict the direction (taken, not taken) for conditional branches
	*/
	if( MD_OP_FLAGS(op) & F_COND ) {
		g_total_cond_branches++;
		unsigned index = (regs.regs_PC >> 3) & ((1<<TWOBIT_SC_TAG_BITS) -1);
		
		// Note that this time we are using two bit therefore in order to only use 262144 bits we need  262144/2 rows
		assert( index < (TOTAL_SIZE/2) );
		
		//int prediction = (int) (bpred_pht[ index ] & 2); // 2 = 0b0010
		int prediction=0;
		if(bpred_pht[index] > 1)
		{
			prediction = 1;
		}
		else 
		{
			prediction = 0;
		}
		
		/* 
		We check what the correct outcome is by comparing the next PC against the sequentially next PC value. 
		*/
		int actual_outcome=(regs.regs_NPC!=(regs.regs_PC+sizeof(md_inst_t)));
		/*
		If the actual branch outcome computed in Step(e) is different from the predicted outcome in Step(d), we mispredicted the branch.
		*/
		if( prediction != actual_outcome ) {
			g_total_mispredictions++;
		}
		
		if (actual_outcome == 0  ){
		/* 
		If equal zero then that means actual branch was not taken.
		If you are not in state 0b00 then deduct one from the state. 
		*/
			if (bpred_pht[ index ] != 0){
				bpred_pht[ index ]--;
			}
		}else if (actual_outcome == 1){
		/* 
		If equal one then that means actual branch was taken.
		If you are not in state 0b11 then increment the state. 
		*/
			if (bpred_pht[ index ] != 3){
				bpred_pht[ index ]++;
			}
		}
	}
}



/*
This function uses one bit branch prediction method with correlation bit
*/

void OneBit_c_predictor(int * taken_arr, int * notTaken_arr, enum md_opcode op, int * history_bit)
{
	/*
	Note that this method uses two bits of the 
	We only need to predict the direction (taken, not taken) for conditional branches
	*/
	if( MD_OP_FLAGS(op) & F_COND ) {
		g_total_cond_branches++;
		unsigned index = (regs.regs_PC >> 3) & ((1<<ONEBIT_C_TAG_BITS) -1);
		assert( index < (TOTAL_SIZE/2) );
		
		/* 
		We check what the correct outcome is by comparing the next PC against the sequentially next PC value. 
		*/
		int actual_outcome=(regs.regs_NPC!=(regs.regs_PC+sizeof(md_inst_t)));

		int prediction ;
		if ((*history_bit)==0){
			/*The correlation bit is zero, this means use the first column (array) which means using the first bit of integers*/
			prediction= (notTaken_arr[index]); 
			
			/*
			If the actual branch outcome computed in Step(e) is different from the predicted outcome in Step(d), we mispredicted the branch.
			*/
			if( prediction != actual_outcome ) 
			{g_total_mispredictions++;}
			notTaken_arr[ index ] = actual_outcome;
			
		}else if ((*history_bit) == 1 ){
			/*The correlation bit is one, this means use the second column (array) which means using the second bit of integers*/
			prediction =(taken_arr[index]);
			
			/*
			If the actual branch outcome computed in Step(e) is different from the predicted outcome in Step(d), we mispredicted the branch.
			*/
			if( prediction != actual_outcome ) 
			{g_total_mispredictions++;}
			taken_arr[ index ] = (actual_outcome);
		}else {
			// Something went wrong
		}
		
		//Set the history bit
		*history_bit=actual_outcome;
	}
}



void shift_cancat (int * shiftee, int bit)
{
  (*shiftee) = ((*shiftee) >> 1);
  if (bit == 1)
  { 
	(*shiftee) = ((*shiftee) | 8);
  }  
}

/*
Part iv
*/
void TwoBit_FourBitC_predictor ( int ** bpred_2d, enum md_opcode op, int * Four_Bit_C)
{
	if( MD_OP_FLAGS(op) & F_COND ) {
		g_total_cond_branches++;
		unsigned index = (regs.regs_PC >> 3) & ((1<<TWOBIT_4BIT_SC_TAG_BITS) - 1);
		
		// Note that this time we are using two bit therefore in order to only use 262144 bits we need  262144/2 rows
		assert( index < ((TOTAL_SIZE/2)/16) );
		assert((*Four_Bit_C) <16);
		int actual_outcome=(regs.regs_NPC!=(regs.regs_PC+sizeof(md_inst_t)));

		int prediction ;
		int ii;
		
		for (ii = 0 ; ii <16 ; ii++ )
		{
			if ((*Four_Bit_C) == ii){
				prediction= (bpred_2d[index][*Four_Bit_C]);
				assert((*Four_Bit_C) <16);
				if (actual_outcome == 0  ){
					if (bpred_2d[index][*Four_Bit_C] != 0){
						bpred_2d[index][*Four_Bit_C]--;
					}
					shift_cancat(Four_Bit_C,0 );
					
					
				}else if (actual_outcome == 1){
					if (bpred_2d[index][*Four_Bit_C] != 3){
						bpred_2d[index][*Four_Bit_C]++;
					}
					shift_cancat(Four_Bit_C ,1);
				}

			}
		}
		if( prediction != actual_outcome ) 
		{g_total_mispredictions++;}

		bpred_2d[index][*Four_Bit_C] = actual_outcome;
		
	}
}

void shift_cancat_17 (int * shiftee, int bit)
{
  (*shiftee) = ((*shiftee) >> 1);
  if (bit == 1)
  { 
	(*shiftee) = ((*shiftee) | 65536);
	//(*shiftee) = ((*shiftee) +2048);
	
  }  
}


/*
This function uses gshare branch prediction method
*/
void gshare_predictor(int * bpred_pht, enum md_opcode op, int *  History_bits)
{
// Add 
	/*
	We only need to predict the direction (taken, not taken) for conditional branches
	*/
	if( MD_OP_FLAGS(op) & F_COND ) {
		g_total_cond_branches++;
		/*  
		index used to lookup a 2-bit counter uses “PC-bits from 3(ii)” bitwise-XOR “some number of bits of branch history”
		*/
		
		//unsigned index = (regs.regs_PC) ^ (*History_bits);
		//index = index & ((1<<GSHARE_TAG_BITS)-1);
		unsigned index = ((regs.regs_PC >> 3) & ((1<< GSHARE_TAG_BITS ) -1)) ^ (*History_bits);
		
		// Note that this time we are using two bit therefore in order to only use 262144 bits we need  262144/2 rows
		assert( index < (TOTAL_SIZE/2) );
		
		//int prediction = (bpred_pht[ index ] & 2); // 2 = 0b0010
		int prediction=0;
		if(bpred_pht[index] > 1)
		{
			prediction = 1;
		}
		else 
		{
			prediction = 0;
		}
		/* 
		We check what the correct outcome is by comparing the next PC against the sequentially next PC value. 
		*/
		int actual_outcome=(regs.regs_NPC!=(regs.regs_PC+sizeof(md_inst_t)));
		/*
		If the actual branch outcome computed in Step(e) is different from the predicted outcome in Step(d), we mispredicted the branch.
		*/
		if( prediction != actual_outcome ) {
			g_total_mispredictions++;
		}
		
		if (actual_outcome == 0  ){
		/* 
		If equal zero then that means actual branch was not taken.
		If you are not in state 0b00 then deduct one from the state. 
		*/
			if (bpred_pht[ index ] != 0){
				bpred_pht[ index ]--;
			}
			shift_cancat_17(History_bits ,0);
		}else if (actual_outcome == 1){
		/* 
		If equal one then that means actual branch was taken.
		If you are not in state 0b11 then increment the state. 
		*/
			if (bpred_pht[ index ] != 3){
				bpred_pht[ index ]++;
			}
			shift_cancat_17(History_bits ,1);
		}
	}
}



/* start simulation, program loaded, processor precise state initialized */
void
sim_main(void)
{
	//predictor table
#if OneBit_predictor_flag==TRUE
	int bpred_pht[TOTAL_SIZE];
#elif TwoBit_sc_predictor_flag==TRUE
	int bpred_pht[TOTAL_SIZE/2];
#elif OneBit_c_predictor_flag==TRUE 
	int taken_arr [TOTAL_SIZE/2];
	int notTaken_arr [TOTAL_SIZE/2];
	// Correlation bit 
	int history_bit = 0;
#elif TwoBit_FourBitC_predictor_flag==TRUE
	int** TwoD_arr;
	int col = 16;
	int row = ((TOTAL_SIZE/2)/col);
    TwoD_arr = (int**) malloc(row * sizeof(int*));
	int i=0;
	for (i = 0; i < row; i++)
    {
        TwoD_arr[i] = (int*) calloc(col,sizeof(int));
    }

	int history_FourBit = 0;
#elif gshare_predictor_flag==TRUE
	int bpred_pht[TOTAL_SIZE/2];
	int history_FourBit = 0;
#endif 

	md_inst_t inst;
	register md_addr_t addr;
	enum md_opcode op;
	register int is_write;
	enum md_fault_type fault;

	fprintf(stderr, "sim: ** starting functional simulation **\n");

	/* set up initial default next PC */
	regs.regs_NPC = regs.regs_PC + sizeof(md_inst_t);


	while (TRUE)
	{
		/* maintain $r0 semantics */
		regs.regs_R[MD_REG_ZERO] = 0;
#ifdef TARGET_ALPHA
		regs.regs_F.d[MD_REG_ZERO] = 0.0;
#endif /* TARGET_ALPHA */

		/* get the next instruction to execute */
		MD_FETCH_INST(inst, mem, regs.regs_PC);

		/* keep an instruction count */
		sim_num_insn++;

		/* set default reference address and access mode */
		addr = 0; is_write = FALSE;

		/* set default fault - none */
		fault = md_fault_none;

		/* decode the instruction */
		MD_SET_OPCODE(op, inst);

		/* execute the instruction */
		switch (op)
		{
#define DEFINST(OP,MSK,NAME,OPFORM,RES,FLAGS,O1,O2,I1,I2,I3)		\
			case OP:							\
				SYMCAT(OP,_IMPL);						\
				break;
#define DEFLINK(OP,MSK,NAME,MASK,SHIFT)					\
			case OP:							\
				panic("attempted to execute a linking opcode");
#define CONNECT(OP)
#define DECLARE_FAULT(FAULT)						\
				{ fault = (FAULT); break; }
#include "machine.def"
		default:
			panic("attempted to execute a bogus opcode");
		}

		if (fault != md_fault_none)
		fatal("fault (%d) detected @ 0x%08p", fault, regs.regs_PC);

		if (verbose)
		{
			myfprintf(stderr, "%10n [xor: 0x%08x] @ 0x%08p: ",
			sim_num_insn, md_xor_regs(&regs), regs.regs_PC);
			md_print_insn(inst, regs.regs_PC, stderr);
			if (MD_OP_FLAGS(op) & F_MEM)
			myfprintf(stderr, "  mem: 0x%08p", addr);
			fprintf(stderr, "\n");
			/* fflush(stderr); */
		}

		if (MD_OP_FLAGS(op) & F_MEM)
		{
			sim_num_refs++;
			if (MD_OP_FLAGS(op) & F_STORE)
			is_write = TRUE;
		}


		/*
		We only need to predict the direction (taken, not taken) for conditional branches
		*/
#if OneBit_predictor_flag==TRUE
		OneBit_predictor(bpred_pht,op);
#elif TwoBit_sc_predictor_flag==TRUE
		TwoBit_sc_predictor(bpred_pht,op);
#elif OneBit_c_predictor_flag==TRUE
		OneBit_c_predictor(taken_arr,notTaken_arr,op, &history_bit);
#elif TwoBit_FourBitC_predictor_flag==TRUE 
		TwoBit_FourBitC_predictor(TwoD_arr,op,&history_FourBit);
#elif gshare_predictor_flag==TRUE
		gshare_predictor(bpred_pht,op,&history_FourBit);
#elif test_all==TRUE
// The code below doesn't work however the whole assignment works.
///////////////////////////////////
if( MD_OP_FLAGS(op) & F_COND ) {
	int oneBit_bpred_pht[TOTAL_SIZE];
	OneBit_predictor(oneBit_bpred_pht,op);
	//assert(g_total_cond_branches<0);
	//assert(g_total_mispredictions < 0);
	accuracy[0]= (float)((float)g_total_mispredictions/(float)g_total_cond_branches);
	branches[0]= (g_total_cond_branches);
	misbranches[0]= (g_total_mispredictions);
	//g_total_cond_branches = 0;
	//g_total_mispredictions = 0;
///////////////////////////////////
	int bpred_pht[TOTAL_SIZE/2];
	TwoBit_sc_predictor(bpred_pht,op);
	accuracy[1]= (float)((float)g_total_mispredictions/(float)g_total_cond_branches);
	//assert(g_total_cond_branches>0);
	//assert(g_total_mispredictions>0);  
	//g_total_cond_branches = 0;
	//g_total_mispredictions = 0;
///////////////////////////////////
	int taken_arr [TOTAL_SIZE/2];
	int notTaken_arr [TOTAL_SIZE/2];
	// Correlation bit 
	int history_bit = 0;
	OneBit_c_predictor(taken_arr,notTaken_arr,op, &history_bit);
	//accuracy[2]= (float)(g_total_mispredictions/g_total_cond_branches);
	//assert(g_total_cond_branches>0);
	//assert(g_total_mispredictions>0);
	//g_total_cond_branches = 0;
	//g_total_mispredictions = 0;
///////////////////////////////////
/*	int** TwoD_arr;
	int col = 16;
	int row = ((TOTAL_SIZE/2)/col);
    TwoD_arr = (int**) malloc(row * sizeof(int*));
	int cm=0;
	for (cm = 0; cm < row; cm++)
    {
        TwoD_arr[cm] = (int*) calloc(col,sizeof(int));
    }
	int history_FourBit = 0;
	TwoBit_FourBitC_predictor(TwoD_arr,op,&history_FourBit);
	//accuracy[3]= (float)(g_total_mispredictions/g_total_cond_branches);
	//assert(g_total_cond_branches>0);
	//assert(g_total_mispredictions>0);
	g_total_cond_branches = 0;
	g_total_mispredictions = 0;*/
///////////////////////////////////
	int gshare_bpred_pht[TOTAL_SIZE/2];
	int gshare_history_bit = 0;
	gshare_predictor(gshare_bpred_pht,op,&gshare_history_bit);
	//accuracy[4]= (float)(g_total_mispredictions/g_total_cond_branches);
	//assert(g_total_cond_branches>0);
	//assert(g_total_mispredictions>0);
	//g_total_cond_branches = 0;
	//g_total_mispredictions = 0;
	}
#endif
		
		/* go to the next instruction */
		regs.regs_PC = regs.regs_NPC;
		regs.regs_NPC += sizeof(md_inst_t);
		
		/* finish early? */
		if (max_insts && sim_num_insn >= max_insts)
		return;
	}
}
