# README #

### Quick summary ###

* i. 1-bit predictor (Slide Set 8, Slide 13)

* ii. 2-bit predictor using a 2-bit “saturating counter” finite state machine

* iii. 1-bit predictor with 1 -bit of “branch history” for branch correlation

* iv. 2-bit predictor using a 2-bit saturating counter finite state machine and 4-bits of branch history (Slide Set 8, shows a 2-bit predictor using a 2-bit saturating counter finite state machine but with only 2-bits of branch history—you need to use 4-bits instead; you can shift bits in C using the “>>” and “<<” operators).

* v. A predictor of your own design that achieves better performance than any of the above. A few suggestions are provided in Section 4.


---------------------------------------------------------------------
###Report Submitted###
Jesse Melamed 12424115    
Seyed Ehsan Abrishami  15219116

We correctly implemented all the required branch predictors including: 1-bit predictor, 2-bit predictor with a 2-bit saturating counter, 1-bit predictor with 1-bit of branch history, and a 2-bit predictor with a 2-bit saturating counter as well as 4-bits of branch history. We also completed our own custom Gshare predictor, which you will see was faster than the other required predictors. We compiled our results of individual and average branch prediction accuracy for all 5 predictors we created across all 4 benchmarks. We converted these results into a table and bar chart for ease of viewing, as seen below in Figure 1. 

![graph.PNG](https://bitbucket.org/repo/MeyGKa/images/1840150183-graph.PNG)

Figure 1. All of the results of the 4 benchmarks for each of the 5 predictors are shown in both chart form and table form for easy comparison. BP = Branch predictor, SC = Saturating counter.

##Analysis:##
There are some clear trends when looking at our data. Overall, the ‘go’ benchmark results in the lowest accuracy for all of the predictors while ‘fpppp’ results in the highest accuracy. We can see that relative to each other, all of the accuracies are the same. Meaning that they stay in the same position(most accurate to least accurate) regardless of which benchmark is used. This tells us that our predictors are correct because they are consistent. The most accurate predictor, as we had hoped it would be, is our own custom predictor. The Gshare predictor has an average accuracy of 91.2%. This is almost 5% better than the next closest predictor, a significant improvement. The most accurate predictor of the standard predictors is the 2-bit branch predictor with a 2-bit saturating counter at 87.1%. Next is the 2-bit branch predictor with a 2-bit saturating counter and 4-bits of history at 85.8%. Followed by the 1-bit branch predictor with 1-bit of history with 84.1% accuracy. Finally, the 1-bit branch predictor with no saturating counter or history bits is a lowly 83.0% accurate. 

These findings make sense as the average accuracy of the branch predictor almost directly relates to how complex the predictor is. Obviously the Gshare predictor is the most accurate as it is designed to be a very accurate branch predictor. Disregarding the Gshare branch predictor we can see some more trends in the other predictors.  The 1-bit branch predictor has the lowest level of complexity and consequently has the lowest accuracy. Whereas the 2-bit branch predictor with the 2-bit saturating counter is the most accurate, it has a more in depth branch predictor with the 2-bit saturating counter state machine providing major benefits to its accuracy. interesting to note is that the most complex predictor of the required predictors, the 2-bit branch predictor with both 2-bits of saturating counter and 4-bits of history, is only the second most accurate predictor. This could be due to the fact that it records too much information which evidently does more harm than good. There is a balance to the amount of information needed versus the accuracy. A sufficiently complex branch predictor can be more accurate than a very simple branch predictor and a more complex branch predictor

We are happy with the results we have achieved. We feel that they prove our solutions to each predictor are correct and are a good representation of how each predictor performs. We are also happy with correctly implemented the custom Gshare predictor and that it outperformed all other branch predictors across all of the benchmarks. The results of this project clearly shows how accurate the common branch predictors are. It also shows how much each special feature(i.e. a saturating counter, varying bits of history or a combination of both) improves or degrades a branch predictors accuracy. This is shown in the roughly 1% improvement of the 1-bit branch predictor with 1-bit of history compared to the 1-bit branch predictor with no history. The 1% improvement can be attributed to the addition of the 1-bit of history. This project was a good learning tool for understanding how branch predictors work.

##Appendix: Design Discussion##
Because we had constraint on the memory space used by the program we had to reduce the size of the tables we used. In particular:

> The table size for one bit predictor (part i) was 262144. The number of used tag bits from Program Counter is log_2 (262144)=18  
> The table size for two bit saturating counter predictor (part ii) was 262144/2=131072. The tag bits used is log_2 (131072) = 17  
> The table size for one bit predictor with history bit (part iii) was 262144/2=131072. The tag bits used is log_2 (131072) = 17  
> The table size for 2-bit predictor using a 2-bit saturating counter and 4-bits of branch history (part iv) was 262144/2/16=8192. The tag bits used is log_2 (8192) = 13.  
> The table size for gshare predictor (part v) was 262144/2=131072. The tag bits used is log_2 (131072) = 17.